﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Suri.ViewModel
{
    public class Friend
    {
        public string id { get; set; }
        public string Name { get; set; }
        public Uri PictureUri { get; set; }
    }

    public class Roomate : Friend
    {  }

    public class FacebookData
    {
        private static ObservableCollection<Roomate> roomate = new ObservableCollection<Roomate>();
        public static ObservableCollection<Roomate> Roomate
        {
           get
           {
                return roomate;
           }
        }

        private static ObservableCollection<Friend> friends = new ObservableCollection<Friend>();
        public static ObservableCollection<Friend> Friends
        {
            get
            {
                return friends;
            }
        }

        private static ObservableCollection<Friend> selectedFriends = new ObservableCollection<Friend>();

        public static ObservableCollection<Friend> SelectedFriends
        {
            get
            {
                return selectedFriends;
            }
        }
    }


}
