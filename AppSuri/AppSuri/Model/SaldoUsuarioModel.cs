﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppSuri.Model
{
    [Table]
    class SaldoUsuarioModel : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private string _saldoId;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public string SaldoId
        {
            get
            {
                return _saldoId; 
            }
            set
            {
                if (_saldoId == null)
                {
                    NotifyPropertyChanging("SaldoId");
                    _saldoId = value;
                    NotifyPropertyChanged("SaldoId");
                }
            }
        }

         private string _grupoId;
         [Column]
         public string GrupoId
         {
             get
             {
                 return _grupoId; 
             }
             set
             {
                 if(_grupoId != value)
                 {
                    NotifyPropertyChanging("GrupoId");
                    _grupoId = value;
                    NotifyPropertyChanged("GrupoId");
                 }
             }
         }
         private string _usuarioDevendoId;
         [Column]
         public string UsuarioDevendoId
         {
             get 
             { 
                 return _usuarioDevendoId; 
             }
             set
             {
                 if (_usuarioDevendoId != value)
                 {
                     NotifyPropertyChanging("UsuarioDevendoId");
                     _usuarioDevendoId = value;
                     NotifyPropertyChanged("UsuarioDevendoId");
                 }
             }
         }
         private string _usuarioPagouId;
         [Column]
         public string UsuarioPagouId
         {
             get
             {
                 return _usuarioPagouId; 
             }
             set 
             {
                 if (_usuarioPagouId != value)
                 {
                     NotifyPropertyChanging("UsuarioPagouId");
                     _usuarioPagouId = value;
                     NotifyPropertyChanged("UsuarioPagouId");
                 }
             }
         }

         private double _valor;
         [Column]
         public double Valor
         {
             get 
             {
                 return _valor; 
             }
             set
             {               
                 _valor = value; 
             }
         }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
