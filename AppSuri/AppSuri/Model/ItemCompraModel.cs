﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppSuri.Model
{
    [Table]
    class ItemCompraModel : INotifyPropertyChanged, INotifyPropertyChanging
    {        
        private string _compraItemId;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public string CompraItemId
        {
            get 
            { 
                return _compraItemId; 
            }
            set 
            {
                if (_compraItemId == null)
                {
                    NotifyPropertyChanged("CompraItemId");
                    _compraItemId = value;
                    NotifyPropertyChanged("CompraItemId");
                }
            }
        }

        private string _nomeCompra;
        [Column]
        public string NomeCompra
        {
            get 
            { 
                return _nomeCompra; 
            }
            set 
            {
                if (_nomeCompra == null)
                {
                    NotifyPropertyChanging("NomeCompra");
                    _nomeCompra = value;
                    NotifyPropertyChanged("NomeCompra");
                }
            }
        }

        private double _valorUnidade;
        [Column]
        public double ValorUnidade
        {
            get 
            {
                return _valorUnidade; 
            }
            set
            {
                NotifyPropertyChanging("ValorUnidade");
                _valorUnidade = value;
                NotifyPropertyChanged("ValorUnidade");
            }
        }

        private bool _estaPago;
        [Column]
        public bool EstaPago
        {
            get 
            {
                return _estaPago; 
            }
            set
            {
                if (_estaPago != value)
                {
                    NotifyPropertyChanging("EstaPago");
                    _estaPago = value;
                    NotifyPropertyChanged("EstaPago");
                }
            }
        }

        private string _compraId;
        [Column]
        public string CompraId
        {
            get
            {
                return _compraId; 
            }
            set
            {
                if (_compraItemId == null)
                {
                    NotifyPropertyChanging("CompraId");
                    _compraId = value;
                    NotifyPropertyChanged("CompraId");
                }
            }
        }
        private int _quantidade;
        [Column]
        public int Quantidade
        {
            get
            {
                return _quantidade; 
            }
            set
            {
                NotifyPropertyChanging("Quantidade");
                _quantidade = value;
                NotifyPropertyChanged("Quantidade");
            }
        }
        //pasta querem


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
