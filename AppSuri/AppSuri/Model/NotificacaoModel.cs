﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace AppSuri.Model
{
    [Table]
    class NotificacaoModel : INotifyPropertyChanged, INotifyPropertyChanging
    {        
        private string notificacaoId;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public string NotificacaoId
        {
            get
            { 
                return notificacaoId;
            }
            set 
            {
                NotifyPropertyChanging("NotificacaoId");
                notificacaoId = value;
                NotifyPropertyChanged("NotificacaoId");
            }
        }

        private string grupoId;
        [Column]
        public string GrupoId
        {
            get
            { 
                return grupoId; 
            }
            set 
            {
                NotifyPropertyChanging("GrupoId");
                grupoId = value;
                NotifyPropertyChanged("GrupoId");
            }
        }

        private string usuarioAlvo;
        [Column]
        public string UsuarioAlvo
        {
            get 
            {
                return usuarioAlvo; 
            }
            set
            {
                usuarioAlvo = value; 
            }
        }

        private string tipo;
        [Column]
        public string Tipo
        {
            get 
            {
                return tipo; 
            }
            set
            {
                NotifyPropertyChanging("Tipo");
                tipo = value;
                NotifyPropertyChanged("Tipo");
            }
        }

        private Image _imagemNotificacao;

        public Image ImagemNotificacao
        {
            get
            {
                if (this._imagemNotificacao == null)
                    this._imagemNotificacao = new Image();
                this._imagemNotificacao.Source = new BitmapImage(new Uri(_linkImagemNotificacao));
                return _imagemNotificacao;
            }
            set { _imagemNotificacao = value; }
        }

        private string _linkImagemNotificacao;

        [Column]
        public string LinkImagemNotificacao
        {
            get
            {
                return _linkImagemNotificacao;
            }
            set
            {
                _linkImagemNotificacao = value;
            }
        }


        private string descricao;
        [Column]
        public string Descricao
        {
            get 
            {
                return descricao; 
            }
            set 
            {
                NotifyPropertyChanging("Descricao");
                descricao = value;
                NotifyPropertyChanged("Descricao");
            }
        }

        private string horarioCriacao;
        [Column]
        public string HorarioCriacao
        {
            get 
            { 
                return horarioCriacao; 
            }
            set 
            {
                NotifyPropertyChanging("HorarioCriacao");
                horarioCriacao = value;
                NotifyPropertyChanged("HorarioCriacao");
            }
        }
        private string dataCriacao;
        [Column]
        public string DataCriacao
        {
            get 
            { 
                return dataCriacao;
            }
            set 
            {
                NotifyPropertyChanging("DataCriacao");
                dataCriacao = value;
                NotifyPropertyChanged("DataCriacao");
            }
        }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
