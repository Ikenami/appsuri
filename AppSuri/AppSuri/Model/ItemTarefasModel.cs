﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppSuri.Model
{
    [Table]
    class ItemTarefasModel : INotifyPropertyChanged, INotifyPropertyChanging
    {        
        private string tarefaItemId;
        [Column]
        public string TarefaItemId
        {
            get 
            { 
                return tarefaItemId; 
            }
            set 
            {
                tarefaItemId = value; 
            }
        }
        private string nomeItem;
        [Column]
        public string NomeItem
        {
            get
            { 
                return nomeItem;
            }
            set 
            {
                nomeItem = value; 
            }
        }
        private string observacao;
        [Column]
        public string Observacao
        {
            get 
            { 
                return observacao; 
            }
            set
            { 
                observacao = value; 
            }
        }
        private Boolean estado;
        [Column]
        public Boolean Estado
        {
            get 
            { 
                return estado; 
            }
            set 
            {
                estado = value; 
            }
        }
        private Boolean repeticao; //verificar como vai usar o radio button, dependendo precisa mudar o tipo da variavel
        [Column]
        public Boolean Repeticao
        {
            get
            { 
                return repeticao;
            }
            set 
            {
                repeticao = value; 
            }
        }
        private string UsuarioItemId;
        [Column]
        public string UsuarioItemId1
        {
            get 
            { 
                return UsuarioItemId; 
            }
            set 
            { 
                UsuarioItemId = value; 
            }
        }
        private string tarefaId;

        public string TarefaId
        {
            get 
            { 
                return tarefaId;
            }
            set 
            { 
                tarefaId = value; 
            }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
