﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppSuri.Model
{
    [Table]
    class ItemContaModel : INotifyPropertyChanged, INotifyPropertyChanging
    {        
        private string contaItemId;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public string ContaItemId
        {
            get
            { 
                return contaItemId; 
            }
            set 
            {
                if (contaItemId != value)
                {
                    NotifyPropertyChanging("ContaItemId");
                    contaItemId = value;
                    NotifyPropertyChanged("ContaItemId");
                }
            }
        }

        private string nome;
        [Column]
        public string Nome
        {
            get 
            { 
                return nome; 
            }
            set 
            {
                NotifyPropertyChanging("Nome");
                nome = value;
                NotifyPropertyChanged("Nome");
            }
        }
        private bool estado;
        [Column]
        public bool Estado
        {
            get 
            { 
                return estado; 
            }
            set 
            {
                //ver como tratar o estado da conta nesse caso
                NotifyPropertyChanging("Estado");
                estado = value;
                NotifyPropertyChanged("Estado");
            }
        }
        private string responsavel;
        [Column]
        public string Responsavel
        {
            get 
            { 
                return responsavel; 
            }
            set 
            {
                if (responsavel == null)
                {
                    NotifyPropertyChanging("Responsavel");
                    responsavel = value;
                    NotifyPropertyChanged("Responsavel");
                }
            }
        }
        //verificar como tratar, visto que há dois tipos de conta    
        private Mercado tipoMercado = null;

        internal Mercado TipoMercado
        {
            get
            { 
                return tipoMercado; 
            }
            set 
            {
                if (tipoGeral != null)
                    throw new Exception("Tipo Geral ja esta configurado");
                if(tipoMercado == null)
                    tipoMercado = new Mercado(); 
            }
        }
        private Geral tipoGeral = null;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }


    class Mercado
    {
        private List<string> itens; // substituir string pela tabela de itens
        private double valor;
        private List<UsuarioModel> quemDivide;
        private enum isOk
        {
            Finalizar,
            Dividir,
            Saldo
        }
    }

    class Geral
    {
        private int codigo; // string ou int
        private double valor;
        private List<UsuarioModel> pessoasComp;
    }
}
