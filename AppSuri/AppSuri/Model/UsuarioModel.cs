﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.ComponentModel;
using Facebook;

namespace AppSuri.Model
{
    //ola
    [Table]
    public class UsuarioModel : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private string _usuarioId;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public string UsuarioId
        {
            get 
            {
                return _usuarioId; 
            }
            set 
            {

                // verificar se o Id já foi usado (usuário já cadastrado)
                _usuarioId = value; 

                if(_usuarioId != value)
                {
                    NotifyPropertyChanging("UsuarioId");
                    _usuarioId = value;
                    NotifyPropertyChanged("UsuarioId");
                }               
            }
        }

        private string _nome;
        [Column]
        
        public string Nome
        {
            get 
            {
                return _nome; 
            }
            set 
            {
                if (_nome != value)
                {
                    NotifyPropertyChanging("Nome");
                    _nome = value;
                    NotifyPropertyChanged("Nome");
                }
            }
        }
     

        private string _sobrenome;
        [Column]

        public string Sobrenome
        {
            get
            {
                return _sobrenome;
            }
            set
            {
                if (_sobrenome != value)
                {
                    NotifyPropertyChanging("Sobrenome");
                    _sobrenome = value;
                    NotifyPropertyChanged("Sobrenome");
                }
            }
        }

       
        private string _linkFoto;

        [Column]
        public string LinkFoto
        {
            get
            {
                return _linkFoto;
            }
            set
            {
                _linkFoto = value;
            }
        }

        private Image _foto;
        public Image Foto 
        {
            get
            {
                if (_foto == null)
                    _foto = new Image();
                _foto.Source = new BitmapImage(new Uri(_linkFoto));
                return _foto;
            }
            set
            {
                _foto = value;
            } 
        }

        private string email;

        [Column]
        public string Email
        {
            get 
            { 
                return email; 
            }
            set
            {
                if (email != value)
                {
                    NotifyPropertyChanging("Email");
                    email = value;
                    NotifyPropertyChanged("Email");
                }
            }
        }

        
        private string aniversario;

        [Column]
        public string Aniversario
        {
            get 
            { 
                return aniversario; 
            }
            set
            {
                if (aniversario != value)
                {
                    NotifyPropertyChanging("Aniversario");
                    aniversario = value;
                    NotifyPropertyChanged("Aniversario");
                }
            }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
