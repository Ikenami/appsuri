﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Facebook;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace AppSuri.Model
{
    [Table]
    public class GrupoModel : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _grupoId;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int GrupoID
        {
            get
            {
                return _grupoId;
            }
            set
            {
                if (_grupoId != value)
                {
                    NotifyPropertyChanging("GrupoId");
                    _grupoId = value;
                    NotifyPropertyChanged("GrupoId");
                }
            }
        }


        private string _nomeGrupo;

        [Column]
        public string NomeGrupo
        {
            get
            {
                return _nomeGrupo;
            }
            set
            {
                NotifyPropertyChanging("NomeGrupo");
                _nomeGrupo = value;
                NotifyPropertyChanged("NomeGrupo");
            }
        }

        private Image _imagemGrupo;

        public Image ImagemGrupo
        {
            get 
            {
                if (this._imagemGrupo == null)
                    this._imagemGrupo = new Image();
                this._imagemGrupo.Source = new BitmapImage(new Uri(_linkImagem));
                return _imagemGrupo; 
            }
            set { _imagemGrupo = value; }
        }

        private string _linkImagem;

        [Column]
        public string LinkImagem
        {
            get { return _linkImagem; }
            set 
            {
                _linkImagem = value;
            }
        }


        private string _senha;

        [Column]
        public string Senha
        {
            get
            {
                return _senha;
            }
            set
            {
                NotifyPropertyChanging("Senha");
                _senha = value;
                NotifyPropertyChanged("Senha");
            }
        }

        private string _codigo;

        [Column]
        public string Codigo
        {
            get
            {
                return _codigo;
            }
            set
            {
                NotifyPropertyChanging("Codigo");
                _codigo = value;
                NotifyPropertyChanged("Codigo");
            }
        }

        private string _adminId;

        [Column]
        public string AdminId
        {
            get
            {
                return _adminId;
            }
            set
            {
                NotifyPropertyChanging("AdminId");
                _adminId = value;
                NotifyPropertyChanged("AdminId");
            }
        }

        private string _viceAdminId;

        [Column]
        public string ViceAdminId
        {
            get
            {
                return _viceAdminId;
            }
            set
            {
                NotifyPropertyChanging("ViceAdminId");
                _viceAdminId = value;
                NotifyPropertyChanged("ViceAdminId");
            }
        }

        //incluir tambem "pasta" moradores

        #region INotifyPropertyChanged & INotifyPropertyChanging Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        public void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
